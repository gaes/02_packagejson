# Using a package.json #

The best way to manage locally installed npm packages is to create a package.json file.

A package.json file affords you a lot of great things:

* It serves as documentation for what packages your project depends on.
* It allows you to specify the versions of a package that your project can use using semantic versioning rules.
* Makes your build reproducible which means that its way easier to share with other developers.


 https://docs.npmjs.com/getting-started/using-a-package.json

npm install